const express = require('express');
const router = express.Router();

var mysqlDump = require('mysqldump');

router.get('/backup', (req, res, next) => {

  mysqlDump({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'asophie',
    dest: './backup/' + (new Date().getTime()).toString() + '.sql' // destination file 
  }, function (err) {
    if (err) throw err;
    res.send('done');
  });

});



module.exports = router;