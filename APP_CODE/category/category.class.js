const express = require('express');
const router = express.Router();
const db = require('../../config/database');

let Category = {
    /**
     * get the category by Id
     * @param id category id 
     * @param res express response object
     */
    getCategoryById: (id, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('SELECT * FROM category WHERE ID_CATEGORY=?', [id], (err, result, fields) => {
                if (err) throw err;
                res.send(result);
            });
        });
    },

    /**
     * get all categories 
     * @param res express response object
     */
    getCategories: (res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('SELECT * FROM category', (err, result, fields) => {
                if (err) throw err;
                res.send(result);
            });
        });
    },

    /**
     * delete category by id 
     * @param id int category id
     * @param res express response object 
     */
    deleteCategory: (id, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('DELETE FROM category WHERE ID_CATEGORY=?', [id], (err, result) => {
                if (err) throw err;
                if (result.affectedRows) {
                    res.send({
                        "success": true
                    });
                } else {
                    res.send({
                        "success": false,
                        "result": result
                    });
                }
            });
        });
    },

    /**
     * update category info
     * @param catInfo JSON object
     * @param id int category id
     * @param res express response object
     */
    updateCategory: (catInfo, id, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('UPDATE category SET ? WHERE ID_CATEGORY=?', [cateInfo, id], (err, result) => {
                if (err) throw err;
                if (result.affectedRows) {
                    res.send({
                        "success": true
                    });
                } else {
                    res.send({
                        "success": false,
                        "result": result
                    });
                }
            });
        });
    },
    /**
     * insert new category info
     * @param catInfo JSON object
     * @param res express response object
     */
    insertCategory: (catInfo, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('INSERT INTO category (name) VALUES(?)', [cateInfo], (err, result) => {
                if (err) throw err;
                if (result.affectedRows) {
                    res.send({
                        "success": true
                    });
                } else {
                    res.send({
                        "success": false,
                        "result": result
                    });
                }
            });
        });
    }

};

module.exports = Category;