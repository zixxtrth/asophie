const express = require('express');
const Category = require('./category.class');
let route = express.Router();

// get the category by id
route.get('/getCategoryById/:id', (req, res, next) => {
    Category.getCategoryById(req.params.id, res);
});

// get all the categories
route.get('/getCategories/', (req, res, next) => {
    Category.getCategories(res);
});

// add new category
route.post('/addCategory/', (req, res, next) => {
    var name = req.body.name;
    var catInfo = [name];
    Category.insertCategory(catInfo, res);
});

// update category by id 
route.put('/updateCategory/:id/', (req, res, next) => {
    Category.updateCategory(JSON.parse(req.body.catInfo), {
        ID_CATEGORY: req.params.id
    }, res);
});

// delete category by id 
route.delete('/deleteCategory/', (req, res, next) => {
    Category.deleteCategory(req.body.id, res)
});


module.exports = route;