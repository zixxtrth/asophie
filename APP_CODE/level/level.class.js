const express = require('express');
const router = express.Router();
const db = require('../../config/database');

let Level = {
    /**
     * get the level by Id
     * @param id level id 
     * @param res express response object
     */
    getlevelById: (id, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('SELECT * FROM level WHERE id=?', [id], (err, result, fields) => {
                if (err) throw err;
                res.send(result);
            });
        });
    },

    /**
     * get all levels 
     * @param res express response object
     */
    getlevels: (res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('SELECT * FROM level', (err, result, fields) => {
                if (err) throw err;
                res.send(result);
            });
        });
    },

    /**
     * delete level by id 
     * @param id int level id
     * @param res express response object 
     */
    deletelevels: (id, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('DELETE FROM level WHERE id=?', [id], (err, result) => {
                if (err) throw err;
                if (result.affectedRows) {
                    res.send({
                        "success": true
                    });
                } else {
                    res.send({
                        "success": false,
                        "result": result
                    });
                }
            });
        });
    },

    /**
     * update level info
     * @param levelInfo JSON object
     * @param id int level id
     * @param res express response object
     */
    updatelevel: (levelInfo, id, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('UPDATE level SET ? WHERE id=?', [levelInfo, id], (err, result) => {
                if (err) throw err;
                if (result.affectedRows) {
                    res.send({
                        "success": true
                    });
                } else {
                    res.send({
                        "success": false,
                        "result": result
                    });
                }
            });
        });
    },
    /**
     * insert new level info
     * @param levelInfo JSON object
     * @param res express response object
     */
    insertlevel: (levelInfo, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('INSERT INTO level (title, rank) VALUES(?)', [levelInfo], (err, result) => {
                if (err) throw err;
                if (result.affectedRows) {
                    res.send({
                        "success": true
                    });
                } else {
                    res.send({
                        "success": false,
                        "result": result
                    });
                }
            });
        });
    }

};

module.exports = Level;