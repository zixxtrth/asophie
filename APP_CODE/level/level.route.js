const express = require('express');
const Level = require('./level.class');
let route = express.Router();

// get the Level by id
route.get('/getLevelById/:id', (req, res, next) => {
    Level.getLevelById(req.params.id, res);
});

// get all the categories
route.get('/getLevels/', (req, res, next) => {
    Level.getLevels(res);
});

// add new Level
route.post('/addLevel/', (req, res, next) => {
    var title = req.body.name;
    var rank = req.body.rank;
    var levelInfo = [title, rank];
    Level.insertLevel(levelInfo, res);
});

// update Level by id 
route.put('/updateLevel/:id/', (req, res, next) => {
    Level.updateLevel(JSON.parse(req.body.levelInfo), {
        id: req.params.id
    }, res);
});

// delete Level by id 
route.delete('/deleteLevel/', (req, res, next) => {
    Level.deleteLevel(req.body.id, res)
});


module.exports = route;