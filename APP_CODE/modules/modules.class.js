const express = require('express');
const router = express.Router();
const db = require('../../config/database');

let Modules = {
    /**
     * get the modules by Name
     * @param Name modules Name 
     * @param res express response object
     */
    getModuleByName: (name, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('SELECT * FROM modules WHERE name=?', [name], (err, result, fields) => {
                if (err) throw err;
                res.send(result);
            });
        });
    },
    /**
     * get the modules child
     * @param Name modules Name 
     * @param res express response object
     */
    getModuleChildes: (parent, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('SELECT * FROM modules WHERE parent=?', [parent], (err, result, fields) => {
                if (err) throw err;
                res.send(result);
            });
        });
    },

    /**
     * get all moduless 
     * @param res express response object
     */
    getModules: (res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('SELECT * FROM modules', (err, result, fields) => {
                if (err) throw err;
                res.send(result);
            });
        });
    },

    /**
     * delete modules by id 
     * @param id int modules id
     * @param res express response object 
     */
    deleteModuless: (id, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('DELETE FROM modules WHERE id=?', [id], (err, result) => {
                if (err) throw err;
                if (result.affectedRows) {
                    res.send({
                        "success": true
                    });
                } else {
                    res.send({
                        "success": false,
                        "result": result
                    });
                }
            });
        });
    },

    /**
     * update modules info
     * @param modulesInfo JSON object
     * @param id int modules id
     * @param res express response object
     */
    updateModules: (modulesInfo, id, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('UPDATE modules SET ? WHERE id=?', [modulesInfo, id], (err, result) => {
                if (err) throw err;
                if (result.affectedRows) {
                    res.send({
                        "success": true
                    });
                } else {
                    res.send({
                        "success": false,
                        "result": result
                    });
                }
            });
        });
    },
    /**
     * insert new modules info
     * @param modulesInfo JSON object
     * @param res express response object
     */
    insertModules: (modulesInfo, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('INSERT INTO modules (name, title, link,parent,  level) VALUES(?)', [modulesInfo], (err, result) => {
                if (err) throw err;
                if (result.affectedRows) {
                    res.send({
                        "success": true
                    });
                } else {
                    res.send({
                        "success": false,
                        "result": result
                    });
                }
            });
        });
    }

};

module.exports = Modules;