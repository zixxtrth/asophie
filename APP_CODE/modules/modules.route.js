const express = require('express');
const Modules = require('./modules.class');
let route = express.Router();

// get the Module by id
route.get('/getModuleByName/:name', (req, res, next) => {
    Modules.getModuleByName(req.params.name, res);
});
// get the Module by id
route.get('/getModuleChildes/:name', (req, res, next) => {
    Modules.getModuleChildes(req.params.name, res);
});

// get all the categories
route.get('/getModules/', (req, res, next) => {
    Modules.getModules(res);
});

// add new Modules
route.post('/addModules/', (req, res, next) => {
    var name = req.body.name;
    var title = req.body.title;
    var link = req.body.link;
    var level = req.body.level;
    var modulesInfo = [name, title, link, level];
    Modules.insertModules(modulesInfo, res);
});

// update Modules by id 
route.put('/updateModules/:id/', (req, res, next) => {
    Modules.updateModules(JSON.parse(req.body.modulesInfo), {
        id: req.params.id
    }, res);
});

// delete Modules by id 
route.delete('/deleteModules/', (req, res, next) => {
    Modules.deleteModules(req.body.id, res)
});


module.exports = route;