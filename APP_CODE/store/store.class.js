const express = require('express');
const router = express.Router();
const db = require('../../config/database');

let Store = {
    /**
     * get the Store by Id
     * @param id Store id 
     * @param res express response object
     */
    getStoreById: (id, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('SELECT * FROM store WHERE id_store=?', [id], (err, result, fields) => {
                if (err) throw err;
                res.send(result);
            });
        });
    },

    /**
     * get all Stores 
     * @param res express response object
     */
    getStores: (res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('SELECT * FROM store', (err, result, fields) => {
                if (err) throw err;
                res.send(result);
            });
        });
    },

    /**
     * delete Store by id 
     * @param id int Store id
     * @param res express response object 
     */
    deleteStore: (id, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('DELETE FROM store WHERE id_store=?', [id], (err, result) => {
                if (err) throw err;
                if (result.affectedRows) {
                    res.send({
                        "success": true
                    });
                } else {
                    res.send({
                        "success": false,
                        "result": result
                    });
                }
            });
        });
    },

    /**
     * update Store info
     * @param storeInfo JSON object
     * @param id int Store id
     * @param res express response object
     */
    updateStore: (storeInfo, id, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('UPDATE store SET ? WHERE id_store=?', [storeInfo, id], (err, result) => {
                if (err) throw err;
                if (result.affectedRows) {
                    res.send({
                        "success": true
                    });
                } else {
                    res.send({
                        "success": false,
                        "result": result
                    });
                }
            });
        });
    },
    /**
     * insert new Store info
     * @param storeInfo JSON object
     * @param res express response object
     */
    insertStore: (storeInfo, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('INSERT INTO store (name_store) VALUES(?)', [storeInfo], (err, result) => {
                if (err) throw err;
                if (result.affectedRows) {
                    res.send({
                        "success": true
                    });
                } else {
                    res.send({
                        "success": false,
                        "result": result
                    });
                }
            });
        });
    }

};

module.exports = Store;