const express = require('express');
const Store = require('./store.class');
let route = express.Router();

// get the Store by id
route.get('/getStoreById/:id', (req, res, next) => {
    Store.getStoreById(req.params.id, res);
});

// get all the categories
route.get('/getStores/', (req, res, next) => {
    Store.getStores(res);
});

// add new Store
route.post('/addStore/', (req, res, next) => {
    var name = req.body.name;
    var storeInfo = [name];
    Store.insertStore(storeInfo, res);
});

// update Store by id 
route.put('/updateStore/:id/', (req, res, next) => {
    Store.updateStore(JSON.parse(req.body.storeInfo), {
        id_store: req.params.id
    }, res);
});

// delete Store by id 
route.delete('/deleteStore/', (req, res, next) => {
    Store.deleteStore(req.body.id, res)
});


module.exports = route;