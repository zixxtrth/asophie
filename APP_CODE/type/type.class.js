const express = require('express');
const router = express.Router();
const db = require('../../config/database');

let Type = {
    /**
     * get the Type by Id
     * @param id Type id 
     * @param res express response object
     */
    getTypeById: (id, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('SELECT * FROM name_type WHERE id=?', [id], (err, result, fields) => {
                if (err) throw err;
                res.send(result);
            });
        });
    },

    /**
     * get all Types 
     * @param res express response object
     */
    getTypes: (res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('SELECT * FROM name_type', (err, result, fields) => {
                if (err) throw err;
                res.send(result);
            });
        });
    },

    /**
     * delete Type by id 
     * @param id int Type id
     * @param res express response object 
     */
    deleteType: (id, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('DELETE FROM name_type WHERE id=?', [id], (err, result) => {
                if (err) throw err;
                if (result.affectedRows) {
                    res.send({
                        "success": true
                    });
                } else {
                    res.send({
                        "success": false,
                        "result": result
                    });
                }
            });
        });
    },

    /**
     * update Type info
     * @param typeInfo JSON object
     * @param id int Type id
     * @param res express response object
     */
    updateType: (typeInfo, id, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('UPDATE name_type SET ? WHERE id=?', [typeInfo, id], (err, result) => {
                if (err) throw err;
                if (result.affectedRows) {
                    res.send({
                        "success": true
                    });
                } else {
                    res.send({
                        "success": false,
                        "result": result
                    });
                }
            });
        });
    },
    /**
     * insert new Type info
     * @param typeInfo JSON object
     * @param res express response object
     */
    insertType: (typeInfo, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('INSERT INTO name_type (name) VALUES(?)', [typeInfo], (err, result) => {
                if (err) throw err;
                if (result.affectedRows) {
                    res.send({
                        "success": true
                    });
                } else {
                    res.send({
                        "success": false,
                        "result": result
                    });
                }
            });
        });
    }

};

module.exports = Type;