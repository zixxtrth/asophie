const express = require('express');
const Type = require('./type.class');
let route = express.Router();

// get the Type by id
route.get('/getTypeById/:id', (req, res, next) => {
    Type.getTypeById(req.params.id, res);
});

// get all the Types
route.get('/getTypes/', (req, res, next) => {
    Type.getTypes(res);
});

// add new Type
route.post('/addType/', (req, res, next) => {
    var name = req.body.name;
    var typeInfo = [name];
    Type.insertType(typeInfo, res);
});

// update Type by id 
route.put('/updateType/:id/', (req, res, next) => {
    Type.updateType(JSON.parse(req.body.typeInfo), {
        id: req.params.id
    }, res);
});

// delete Type by id 
route.delete('/deleteType/', (req, res, next) => {
    Type.deleteType(req.body.id, res)
});


module.exports = route;