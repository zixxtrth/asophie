const db = require('../../config/database');
let Users = {
    
    /**
     *  returns all user info 
     * @param username the username
     * @param password the user password 
     * @param res  express response object
     */
    login: (username, password, res) => {
        db.connect().connect(err => {
            if (err) throw err;
            con.query('SELECT * FROM us000 WHERE name=? AND psw=?', [username, password], function (err, result, fields) {
                if (err) throw err;
                res.send(result);
            });
        });
    },

    /**
     * insert new user 
     * @param userInfo JSON object 
     * @param res  express response object
     */
    addUser: (userInfo, res) => {
        db.connect().connect(function (err) {
            if (err) throw err;
            var sql = "INSERT INTO us000 (name, psw, lvl) VALUES (?)";
            con.query(sql, [
                userInfo
            ], function (err, result) {
                if (err) throw err;
                if (result.affectedRows) {
                    res.send({
                        "success": true
                    });
                } else {
                    res.send({
                        "success": false,
                        "result": result
                    });
                }
            });
        });
    },

    /**
     * delete user from the system
     * @param id int user id
     * @param res  express response object
     */
    deleteUser: (id, res) => {
        db.connect().connect(function (err) {
            if (err) throw err;
            var sql = "DELETE FROM us000 WHERE id = ?";
            con.query(sql, [id], function (err, result) {
                if (err) throw err;
                if (result.affectedRows) {
                    res.send({
                        "success": true
                    });
                } else {
                    res.send({
                        "success": false,
                        "result": result
                    });
                }
            });
        });
    },

    /**
     * update user info 
     * @param newInfo JSON object
     * @param id int user id 
     * @param res express response object
     */
    updateUser: (newInfo, id, res) => {
        db.connect().connect(function (err) {
            if (err) throw err;
            var sql = "UPDATE us000 SET ? WHERE ?";
            con.query(sql, [newInfo, id], function (err, result) {
                if (err) throw err;
                if (result.affectedRows) {
                    res.send({
                        "success": true
                    });
                } else {
                    res.send({
                        "success": false,
                        "result": result
                    });
                }
            });
        });
    },
};

module.exports = Users;