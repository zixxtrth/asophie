const express = require('express');
const User = require('./users.class');
let route = express.Router();
// login request
route.post('/login/', (req, res) => {
    var username = req.body.username;
    var password = req.body.password;
    User.login(username, password, res);
});
// add new user request
route.post('/addUser', (req, res) => {
    var name = req.body.username;
    var psw = req.body.password;
    var lvl = req.body.level;
    var userInfo = [
        name,
        psw,
        lvl
    ];
    User.addUser(userInfo, res);
});
// delete Request
route.delete('/delete/user/', (req, res) => {
    var id = req.body.id;
    User.deleteUser(id, res);
});
// update Request
route.put('/update/user/:id', (req, res) => {
    User.updateUser(JSON.parse(req.body.newInfo), {
        id: req.params.id
    }, res);
});
module.exports = route;