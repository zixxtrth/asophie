const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');

const config = require('./config/database');
const nodethon = require('./APP_CODE/Nodethon/nodethon');

const app = express();
const userRoutes = require('./APP_CODE/users/users.route');
const categoryRoutes = require('./APP_CODE/category/category.route');
const typeRoutes = require('./APP_CODE/type/type.route');
const storeRoutes = require('./APP_CODE/store/store.route');
const modulesRoutes = require('./APP_CODE/modules/modules.route');
const levelRoutes = require('./APP_CODE/level/level.route');
app.use(bodyParser.urlencoded({
  extended: false
}))

// CORS MW
app.use(cors());

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));

// Body Parser MW
app.use(bodyParser.json());

app.use('/nodethon', nodethon);
app.use('/users', userRoutes);
app.use('/category', categoryRoutes);
app.use('/type', typeRoutes);
app.use('/store', storeRoutes);
app.use('/modules', modulesRoutes);
app.use('/level', levelRoutes);
// Invalid Route
app.get('/', (req, res) => {
  res.send('Invalid Endpoint... ');
})

// Start Server process.env.PORT || 
app.listen(3000, () => {
  console.log('Server started...');
})
