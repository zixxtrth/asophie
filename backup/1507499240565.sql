CREATE TABLE IF NOT EXISTS `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num` float NOT NULL,
  `name` varchar(500) NOT NULL,
  `treenum` float NOT NULL,
  `tarhel` text NOT NULL,
  `notc` text NOT NULL,
  `typ` text NOT NULL,
  `tel` varchar(250) NOT NULL,
  `mobil` text NOT NULL,
  `ty` int(11) NOT NULL,
  `debit` float NOT NULL,
  `credit` float NOT NULL,
  `balance` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `accountassembly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assem` text NOT NULL,
  `idacount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `bill1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` int(11) NOT NULL,
  `ID_BILL` int(100) NOT NULL,
  `DATE_BILL` datetime NOT NULL,
  `CURRENCY_BILL` int(11) NOT NULL,
  `VALCURRENCY` float NOT NULL,
  `note` text NOT NULL,
  `TSDID` text NOT NULL,
  `CUSTOMER` int(11) NOT NULL,
  `CASH` int(100) NOT NULL,
  `VAL_CASH` float NOT NULL,
  `HSM` float NOT NULL,
  `TOTAL` float NOT NULL,
  `TRHILE` int(11) NOT NULL,
  `MONEY_RECIPT` float NOT NULL,
  `RMONEY_RECIPT` float NOT NULL,
  `USERE` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `bill2` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_INT` int(11) NOT NULL,
  `ID_BILL` int(11) NOT NULL,
  `ID_ITEM` int(11) NOT NULL,
  `QTY` float NOT NULL,
  `PRICE` float NOT NULL,
  `DISC` float NOT NULL,
  `ADDD` float NOT NULL,
  `NOTE` text NOT NULL,
  `UNIT` varchar(500) NOT NULL,
  `ID_STORE` int(11) NOT NULL,
  `VAL_UNIT` float NOT NULL,
  `MM` float NOT NULL,
  `DD` float NOT NULL,
  `TYP` int(11) NOT NULL,
  `TRHILE` int(11) NOT NULL,
  `show_betweenstore` int(11) NOT NULL,
  `total_currency_1` float NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `category` (
  `ID_CATEGORY` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`ID_CATEGORY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `COMPANY_NAME` varchar(500) NOT NULL,
  `COMPANY_NOTE` text NOT NULL,
  `COMPANY_TEL` varchar(100) NOT NULL,
  `COMPAMY_MOB` varchar(100) NOT NULL,
  `COMPANY_EMAIL` varchar(150) NOT NULL,
  `COMPANY_WEB` varchar(250) NOT NULL,
  `LOGO` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `currency` (
  `id_currency` int(11) NOT NULL AUTO_INCREMENT,
  `name_currency` text NOT NULL,
  `val_currency` float NOT NULL,
  PRIMARY KEY (`id_currency`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `day1` (
  `ID_Day` int(11) NOT NULL AUTO_INCREMENT,
  `Date_Day` datetime NOT NULL,
  `note` text NOT NULL,
  `typ` int(11) NOT NULL,
  `ID_KBD` int(11) NOT NULL,
  `USERE` int(11) NOT NULL,
  PRIMARY KEY (`ID_Day`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `day2` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Day` int(11) NOT NULL,
  `ID_ACCOUNT` float NOT NULL,
  `DEBITE` float NOT NULL,
  `CREDITE` float NOT NULL,
  `NOTE` text NOT NULL,
  `CURRENCY` int(11) NOT NULL,
  `VAL_CURRINCY` float NOT NULL,
  `typ` int(11) NOT NULL,
  `MM` float NOT NULL,
  `DD` float NOT NULL,
  `ID_KBD` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `iteme` (
  `ID_ITEME` int(11) NOT NULL AUTO_INCREMENT,
  `ID_CATEGORY` int(11) NOT NULL,
  `code` text NOT NULL,
  `barcod` varchar(250) NOT NULL,
  `name` varchar(500) NOT NULL,
  `latenename` varchar(500) NOT NULL,
  `note` text NOT NULL,
  `type_iteme` int(11) NOT NULL,
  `name_unit1` varchar(500) NOT NULL,
  `qty_unit1` float NOT NULL,
  `name_unit2` varchar(500) NOT NULL,
  `qty_unit2` float NOT NULL,
  `name_unit3` varchar(600) NOT NULL,
  `qty_unit3` float NOT NULL,
  `price_purshace` float NOT NULL,
  `price_sale` float NOT NULL,
  `qty_min` float NOT NULL,
  `qty_max` float NOT NULL,
  `qty_order` float NOT NULL,
  `img` text NOT NULL,
  `QTY` float NOT NULL,
  `PURSHAE` float NOT NULL,
  PRIMARY KEY (`ID_ITEME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `kbd1` (
  `ID_INT` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Day` int(11) NOT NULL,
  `Date_Day` datetime NOT NULL,
  `note` text NOT NULL,
  `ID_CASH` int(11) NOT NULL,
  `typ` int(11) NOT NULL,
  `USERE` int(11) NOT NULL,
  `status_check` int(11) NOT NULL,
  PRIMARY KEY (`ID_INT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `kbd2` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_INT` int(11) NOT NULL,
  `ID_Day` int(11) NOT NULL,
  `ID_ACCOUNT` int(11) NOT NULL,
  `DEBITE` float NOT NULL,
  `CREDITE` float NOT NULL,
  `NOTE` text NOT NULL,
  `CURRENCY` int(11) NOT NULL,
  `VAL_CURRINCY` float NOT NULL,
  `END_DATE` datetime NOT NULL,
  `typ` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `name_type` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `nsb1` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `nsb2` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_NSB` int(11) NOT NULL,
  `ID_ACCOUNT` int(11) NOT NULL,
  `NSB` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `store` (
  `id_store` int(11) NOT NULL AUTO_INCREMENT,
  `name_store` text NOT NULL,
  PRIMARY KEY (`id_store`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `us000` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `psw` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
