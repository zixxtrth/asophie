db.account
....
        [num] [float] NULL,
	[Name] [nvarchar](max) NULL,
	[treenum] [float] NULL,
	[tarhel] [nvarchar](max) NULL,
	[notc] [nvarchar](max) NULL,
	[typ] [nvarchar](max) NULL,
	[tel] [nvarchar](max) NULL,
	[mobil] [nvarchar](max) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ty] [int] NULL,
	[debit] [float] NULL,
	[credit] [float] NULL,
	[balance] [float] NULL,

db.accountassembly

......
        [id] [int] IDENTITY(1,1) NOT NULL,
	[assem] [nvarchar](max) NULL,
	[idacount] [int] NULL,

db.bill1
.....

	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TYP] [int] NULL,
	[ID_BILL] [int] NULL,
	[DATE_BILL] [datetime] NULL,
	[CURRENCY_BILL] [int] NULL,
	[VALCURRENCY] [float] NULL,
	[NOTE] [nvarchar](500) NULL,
	[TSDID] [nvarchar](500) NULL,
	[CUSTOMER] [int] NULL,
	[CASH] [int] NULL,
	[VAL_CASH] [float] NULL,
	[HSM] [float] NULL,
	[TOTAL] [float] NULL,
	[TRHILE] [int] NULL,
	[MONEY_RECIPT] [float] NULL,
	[RMONEY_RECIPT] [float] NULL,
	[USERE] [int] NULL,
db.bill2
............
        [ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_INT] [int] NULL,
	[ID_BILL] [int] NULL,
	[ID_ITEM] [int] NULL,
	[QTY] [float] NULL,
	[PRICE] [float] NULL,
	[DISC] [float] NULL,
	[ADDD] [float] NULL,
	[NOTE] [nvarchar](500) NULL,
	[UNIT] [nvarchar](500) NULL,
	[ID_STORE] [int] NULL,
	[VAL_UNIT] [float] NULL,
	[MM] [float] NULL,
	[DD] [float] NULL,
	[TYP] [int] NULL,
	[TRHILE] [int] NULL,
	[show_betweenstore] [int] NULL,
	[total_currency_1] [float] NULL,
db.Category
.......
        [ID_CATEGORY] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](200) NULL,

db.COMPANY
.........
        [id] [int] IDENTITY(1,1) NOT NULL,
	[COMPANY_NAME] [nvarchar](max) NULL,
	[COMPANY_NOTE] [nvarchar](max) NULL,
	[COMPANY_TEL] [nvarchar](max) NULL,
	[COMPAMY_MOB] [nvarchar](max) NULL,
	[COMPANY_EMAIL] [nvarchar](max) NULL,
	[COMPANY_WEB] [nvarchar](max) NULL,
	[LOGO] [nvarchar](max) NULL,


db.currency
.................
        [id_currency] [int] IDENTITY(1,1) NOT NULL,
	[name_currency] [nvarchar](200) NULL,
	[val_currency] [float] NULL,

db.day1
...............


        [ID_Day] [int] NOT NULL,
	[Date_Day] [datetime] NULL,
	[note] [nvarchar](500) NULL,
	[typ] [int] NULL,
	[ID_KBD] [int] NULL,
	[USERE] [int] NULL,

db.day2
............
        [ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Day] [int] NULL,
	[ID_ACCOUNT] [float] NULL,
	[DEBITE] [float] NULL,
	[CREDITE] [float] NULL,
	[NOTE] [nvarchar](500) NULL,
	[CURRENCY] [int] NULL,
	[VAL_CURRINCY] [float] NULL,
	[typ] [int] NULL,
	[MM] [float] NULL,
	[DD] [float] NULL,
	[ID_KBD] [int] NULL,

db.iteme
...................



        [ID_ITEME] [int] IDENTITY(1,1) NOT NULL,
	[ID_CATEGORY] [int] NULL,
	[code] [nvarchar](500) NULL,
	[barcod] [nvarchar](500) NULL,
	[name] [nvarchar](500) NULL,
	[latenename] [nvarchar](500) NULL,
	[note] [nvarchar](500) NULL,
	[type_iteme] [int] NULL,
	[name_unit1] [nvarchar](500) NULL,
	[qty_unit1] [float] NULL,
	[name_unit2] [nvarchar](500) NULL,
	[qty_unit2] [float] NULL,
	[name_unit3] [nvarchar](500) NULL,
	[qty_unit3] [float] NULL,
	[price_purshace] [float] NULL,
	[price_sale] [float] NULL,
	[qty_min] [float] NULL,
	[qty_max] [float] NULL,
	[qty_order] [float] NULL,
	[img] [image] NULL,
	[QTY] [float] NULL,
	[PURSHAE] [float] NULL,


db.kbd1
........
        [ID_INT] [int] IDENTITY(1,1) NOT NULL,
	[ID_Day] [int] NULL,
	[Date_Day] [datetime] NULL,
	[note] [nvarchar](500) NULL,
	[ID_CASH] [int] NULL,
	[typ] [int] NULL,
	[USERE] [int] NULL,
	[status_check] [int] NULL,

db.kbd2
..........
        [ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_INT] [int] NULL,
	[ID_Day] [int] NULL,
	[ID_ACCOUNT] [int] NULL,
	[DEBITE] [float] NULL,
	[CREDITE] [float] NULL,
	[NOTE] [nvarchar](500) NULL,
	[CURRENCY] [int] NULL,
	[VAL_CURRINCY] [float] NULL,
	[END_DATE] [datetime] NULL,
	[typ] [int] NULL,

db.name_type
.............

	[id] [int] NOT NULL,
	[name] [nvarchar](max) NULL,
db.NSB1
..............

	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NAME] [nvarchar](max) NULL,
db.NSB2
............

        [ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_NSB] [int] NULL,
	[ID_ACCOUNT] [int] NULL,
	[NSB] [nvarchar](max) NULL,
db.store
...........

        [id_store] [int] IDENTITY(1,1) NOT NULL,
	[name_store] [nvarchar](200) NULL,

db.us000
..........
        [id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NULL,
	[psw] [nvarchar](max) NOT NULL