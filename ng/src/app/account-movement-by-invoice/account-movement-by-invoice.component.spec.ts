import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountMovementByInvoiceComponent } from './account-movement-by-invoice.component';

describe('AccountMovementByInvoiceComponent', () => {
  let component: AccountMovementByInvoiceComponent;
  let fixture: ComponentFixture<AccountMovementByInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountMovementByInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountMovementByInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
