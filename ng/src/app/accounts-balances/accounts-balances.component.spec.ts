import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountsBalancesComponent } from './accounts-balances.component';

describe('AccountsBalancesComponent', () => {
  let component: AccountsBalancesComponent;
  let fixture: ComponentFixture<AccountsBalancesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountsBalancesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountsBalancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
