import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountsGauideComponent } from './accounts-gauide.component';

describe('AccountsGauideComponent', () => {
  let component: AccountsGauideComponent;
  let fixture: ComponentFixture<AccountsGauideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountsGauideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountsGauideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
