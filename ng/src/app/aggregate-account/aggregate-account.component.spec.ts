import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AggregateAccountComponent } from './aggregate-account.component';

describe('AggregateAccountComponent', () => {
  let component: AggregateAccountComponent;
  let fixture: ComponentFixture<AggregateAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AggregateAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AggregateAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
