import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { MainActionMenuComponent } from './main-action-menu/main-action-menu.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { WorkspaceComponent } from './workspace/workspace.component';
import { SubMenuComponent } from './sub-menu/sub-menu.component';
import { ModulesService } from './service/modules.service';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';
import { MainAppComponent } from './main-app/main-app.component';
import { Error404Component } from './error404/error404.component';
import { UserService } from './service/user.service';
import { LoginGuard } from './guards/login.guard';
import { BsDropdownModule } from 'ngx-bootstrap';
import { GeneralSettingsComponent } from './general-settings/general-settings.component';
import { MaterialCardComponent } from './material-card/material-card.component';
import { AccountCardComponent } from './account-card/account-card.component';
import { MovemntsComponent } from './movemnts/movemnts.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { AccountsReportsComponent } from './accounts-reports/accounts-reports.component';
import { BillingReportsComponent } from './billing-reports/billing-reports.component';
import { AuditingComponent } from './auditing/auditing.component';
import { EquationsComponent } from './equations/equations.component';
import { SettingsComponent } from './settings/settings.component';
import { CompanyComponent } from './company/company.component';
import { CompaniesComponent } from './companies/companies.component';
import { CompanyInfoComponent } from './company-info/company-info.component';
import { BackupComponent } from './backup/backup.component';
import { RetrievebackupComponent } from './retrievebackup/retrievebackup.component';
import { WarehouseCardComponent } from './warehouse-card/warehouse-card.component';
import { CategoriesComponent } from './categories/categories.component';
import { MaterialsManagementComponent } from './materials-management/materials-management.component';
import { AccountsGauideComponent } from './accounts-gauide/accounts-gauide.component';
import { CapitalAccountComponent } from './capital-account/capital-account.component';
import { CurrenciesComponent } from './currencies/currencies.component';
import { BanksComponent } from './banks/banks.component';
import { FundboxComponent } from './fundbox/fundbox.component';
import { CustomersComponent } from './customers/customers.component';
import { SuppliersComponent } from './suppliers/suppliers.component';
import { ExpensesComponent } from './expenses/expenses.component';
import { InComesComponent } from './in-comes/in-comes.component';
import { AggregateAccountComponent } from './aggregate-account/aggregate-account.component';
import { RegistrationCertificateComponent } from './registration-certificate/registration-certificate.component';
import { ReceiptVoucherComponent } from './receipt-voucher/receipt-voucher.component';
import { ExchangeVoucherComponent } from './exchange-voucher/exchange-voucher.component';
import { PurchasesBillComponent } from './purchases-bill/purchases-bill.component';
import { BillRecoupedPurchasesComponent } from './bill-recouped-purchases/bill-recouped-purchases.component';
import { SalesBillComponent } from './sales-bill/sales-bill.component';
import { BillOfReturnComponent } from './bill-of-return/bill-of-return.component';
import { FirstTimeStockComponent } from './first-time-stock/first-time-stock.component';
import { PublicJournalComponent } from './public-journal/public-journal.component';
import { AccountStatementComponent } from './account-statement/account-statement.component';
import { AccountsBalancesComponent } from './accounts-balances/accounts-balances.component';
import { FinalAccountsComponent } from './final-accounts/final-accounts.component';
import { DailyMovementOfBillsComponent } from './daily-movement-of-bills/daily-movement-of-bills.component';
import { AccountMovementByInvoiceComponent } from './account-movement-by-invoice/account-movement-by-invoice.component';
import { DailyMovementOfMaterialsComponent } from './daily-movement-of-materials/daily-movement-of-materials.component';
import { MovementMaterialComponent } from './movement-material/movement-material.component';
import { MaterialBalancesComponent } from './material-balances/material-balances.component';
import { ProfitOfMaterialsComponent } from './profit-of-materials/profit-of-materials.component';
import { SalesComponent } from './sales/sales.component';
import { PurchasesForSaleComponent } from './purchases-for-sale/purchases-for-sale.component';
import { InComesAuditingComponent } from './in-comes-auditing/in-comes-auditing.component';
import { WagesComponent } from './wages/wages.component';
import { SpecificAccountComponent } from './specific-account/specific-account.component';
import { NewEquationsComponent } from './new-equations/new-equations.component';
import { ViewEquationsComponent } from './view-equations/view-equations.component';
import { SotresComponent } from './sotres/sotres.component';
import { UsersComponent } from './users/users.component';
import { LevelsComponent } from './levels/levels.component';
import { TypesComponent } from './types/types.component';
import { FileComponent } from './file/file.component';
import { MovementsComponent } from './movements/movements.component';
import { ModulesComponent } from './modules/modules.component';
import { StoresComponent } from './stores/stores.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: 'dashboard', component: MainAppComponent, canActivate: [LoginGuard], children: [
      { path: '', component: WorkspaceComponent },
      { path: 'settings', component: GeneralSettingsComponent },
      {
        path: 'file', component: FileComponent, canActivate: [LoginGuard], children: [
          { path: 'company', component: CompanyComponent },
          { path: 'companies', component: CompaniesComponent },
          { path: 'companyInfo', component: CompanyInfoComponent },
          { path: 'backup', component: BackupComponent },
          { path: 'retrieveBackup', component: RetrievebackupComponent }
        ]
      },
      {
        path: 'materialCard', component: MaterialCardComponent, canActivate: [LoginGuard], children: [
          { path: 'warehouseCard', component: WarehouseCardComponent },
          { path: 'categories', component: CategoriesComponent },
          { path: 'materialsManagement', component: MaterialsManagementComponent },
        ]
      },
      {
        path: 'accountCard', component: AccountCardComponent, canActivate: [LoginGuard], children: [
          { path: 'accountsGuide', component: AccountsGauideComponent },
          { path: 'capitalAccount', component: CapitalAccountComponent },
          { path: 'currencies', component: CurrenciesComponent },
          { path: 'banks', component: BanksComponent },
          { path: 'fundBox', component: FundboxComponent },
          { path: 'customers', component: CustomersComponent },
          { path: 'suppliers', component: SuppliersComponent },
          { path: 'expenses', component: ExpensesComponent },
          { path: 'inComes', component: InComesComponent },
          { path: 'aggregateAccount', component: AggregateAccountComponent }
        ]
      },
      {
        path: 'movements', component: MovementsComponent, canActivate: [LoginGuard], children: [
          { path: 'registrationCertificate', component: RegistrationCertificateComponent },
          { path: 'receiptVoucher', component: ReceiptVoucherComponent },
          { path: 'exchangeVoucher', component: ExchangeVoucherComponent },
        ]
      }
      ,
      {
        path: 'invoices', component: InvoicesComponent, canActivate: [LoginGuard], children: [
          { path: 'purchasesBill', component: PurchasesBillComponent },
          { path: 'billRecoupedPurchases', component: BillRecoupedPurchasesComponent },
          { path: 'salesBill', component: SalesBillComponent },
          { path: 'billOfReturn', component: BillOfReturnComponent },
          { path: 'firstTimeStock', component: FirstTimeStockComponent },
        ]
      }
      ,
      {
        path: 'accountsReports', component: AccountsReportsComponent, canActivate: [LoginGuard], children: [
          { path: 'publicJournal', component: PublicJournalComponent },
          { path: 'accountStatement', component: AccountStatementComponent },
          { path: 'accountsBalances', component: AccountsBalancesComponent },
          { path: 'aggregateAccount', component: AggregateAccountComponent },
          { path: 'finalAccounts', component: FinalAccountsComponent }
        ]
      },
      {
        path: 'billingReports', component: BillingReportsComponent, canActivate: [LoginGuard], children: [
          { path: 'dailyMovementOfBills', component: DailyMovementOfBillsComponent },
          { path: 'accountMovementByInvoice', component: AccountMovementByInvoiceComponent },
          { path: 'movementMaterial', component: MovementMaterialComponent },
          { path: 'materialBalances', component: MaterialBalancesComponent },
          { path: 'profitOfMaterials', component: ProfitOfMaterialsComponent },

        ]
      },
      {
        path: 'auditing', component: AuditingComponent, canActivate: [LoginGuard], children: [
          { path: 'expenses', component: ExpensesComponent },
          { path: 'sales', component: SalesComponent },
          { path: 'customers', component: CustomersComponent },
          { path: 'suppliers', component: SuppliersComponent },
          { path: 'fundBox', component: FundboxComponent },
          { path: 'banks', component: BanksComponent },
          { path: 'purchasesForSale', component: PurchasesForSaleComponent },
          { path: 'inComes', component: InComesAuditingComponent },
          { path: 'wages', component: WagesComponent },
        ]
      },
      {
        path: 'equations', component: EquationsComponent, canActivate: [LoginGuard], children: [
          { path: 'newEquations', component: NewEquationsComponent },
          { path: 'viewEquations', component: ViewEquationsComponent },
        ]
      },
      {
        path: 'settings', component: SettingsComponent, canActivate: [LoginGuard], children: [
          { path: 'modules', component: ModulesComponent },
          { path: 'stores', component: StoresComponent },
          { path: 'levels', component: LevelsComponent },
          { path: 'types', component: TypesComponent },
        ]
      }
]
  },
{ path: '', component: MainAppComponent, canActivate: [LoginGuard] },
{ path: '**', component: Error404Component }
];




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainActionMenuComponent,
    MainMenuComponent,
    WorkspaceComponent,
    SubMenuComponent,
    MainAppComponent,
    Error404Component,
    GeneralSettingsComponent,
    MaterialCardComponent,
    AccountCardComponent,
    MovemntsComponent,
    InvoicesComponent,
    AccountsReportsComponent,
    BillingReportsComponent,
    AuditingComponent,
    EquationsComponent,
    SettingsComponent,
    CompanyComponent,
    CompaniesComponent,
    CompanyInfoComponent,
    BackupComponent,
    RetrievebackupComponent,
    WarehouseCardComponent,
    CategoriesComponent,
    MaterialsManagementComponent,
    AccountsGauideComponent,
    CapitalAccountComponent,
    CurrenciesComponent,
    BanksComponent,
    FundboxComponent,
    CustomersComponent,
    SuppliersComponent,
    ExpensesComponent,
    InComesComponent,
    AggregateAccountComponent,
    RegistrationCertificateComponent,
    ReceiptVoucherComponent,
    ExchangeVoucherComponent,
    PurchasesBillComponent,
    BillRecoupedPurchasesComponent,
    SalesBillComponent,
    BillOfReturnComponent,
    FirstTimeStockComponent,
    PublicJournalComponent,
    AccountStatementComponent,
    AccountsBalancesComponent,
    FinalAccountsComponent,
    DailyMovementOfBillsComponent,
    AccountMovementByInvoiceComponent,
    DailyMovementOfMaterialsComponent,
    MovementMaterialComponent,
    MaterialBalancesComponent,
    ProfitOfMaterialsComponent,
    SalesComponent,
    PurchasesForSaleComponent,
    InComesAuditingComponent,
    WagesComponent,
    SpecificAccountComponent,
    NewEquationsComponent,
    ViewEquationsComponent,
    SotresComponent,
    UsersComponent,
    LevelsComponent,
    TypesComponent,
    FileComponent,
    MovementsComponent,
    ModulesComponent,
    StoresComponent
  ],
  imports: [
    BrowserModule,
    FlashMessagesModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(routes),
    BsDropdownModule.forRoot()
  ],
  providers: [ModulesService, UserService, LoginGuard, Error404Component],
  bootstrap: [AppComponent]
})
export class AppModule { }
