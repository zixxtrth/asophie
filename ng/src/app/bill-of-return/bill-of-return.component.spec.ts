import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillOfReturnComponent } from './bill-of-return.component';

describe('BillOfReturnComponent', () => {
  let component: BillOfReturnComponent;
  let fixture: ComponentFixture<BillOfReturnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillOfReturnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillOfReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
