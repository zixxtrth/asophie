import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillRecoupedPurchasesComponent } from './bill-recouped-purchases.component';

describe('BillRecoupedPurchasesComponent', () => {
  let component: BillRecoupedPurchasesComponent;
  let fixture: ComponentFixture<BillRecoupedPurchasesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillRecoupedPurchasesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillRecoupedPurchasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
