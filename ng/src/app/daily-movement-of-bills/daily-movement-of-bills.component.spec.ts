import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyMovementOfBillsComponent } from './daily-movement-of-bills.component';

describe('DailyMovementOfBillsComponent', () => {
  let component: DailyMovementOfBillsComponent;
  let fixture: ComponentFixture<DailyMovementOfBillsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyMovementOfBillsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyMovementOfBillsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
