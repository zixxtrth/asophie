import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyMovementOfMaterialsComponent } from './daily-movement-of-materials.component';

describe('DailyMovementOfMaterialsComponent', () => {
  let component: DailyMovementOfMaterialsComponent;
  let fixture: ComponentFixture<DailyMovementOfMaterialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyMovementOfMaterialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyMovementOfMaterialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
