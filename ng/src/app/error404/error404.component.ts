import { Component, OnInit, Injectable } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { UserService } from '../service/user.service';
@Injectable()
@Component({
  selector: 'app-error404',
  templateUrl: './error404.component.html',
  styleUrls: ['./error404.component.scss']
})
export class Error404Component implements OnInit {
  private direction: String;
  private directionTitle: String;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit() {

    if (!this.userService.getUserInfo()) {
      this.directionTitle = 'Login';
    } else {
      this.directionTitle = 'Dashboard';

    }
  }
  private changeDirection() {
    if (!this.userService.getUserInfo()) {
      this.router.navigate(['/login']);
    } else {
      this.router.navigate(['/dashboard/']);
    }
  }
  public test() {
    alert('test');
  }

}
