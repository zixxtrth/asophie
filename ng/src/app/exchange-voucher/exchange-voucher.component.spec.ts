import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExchangeVoucherComponent } from './exchange-voucher.component';

describe('ExchangeVoucherComponent', () => {
  let component: ExchangeVoucherComponent;
  let fixture: ComponentFixture<ExchangeVoucherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExchangeVoucherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExchangeVoucherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
