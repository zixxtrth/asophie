import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstTimeStockComponent } from './first-time-stock.component';

describe('FirstTimeStockComponent', () => {
  let component: FirstTimeStockComponent;
  let fixture: ComponentFixture<FirstTimeStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstTimeStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstTimeStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
