import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FundboxComponent } from './fundbox.component';

describe('FundboxComponent', () => {
  let component: FundboxComponent;
  let fixture: ComponentFixture<FundboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FundboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FundboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
