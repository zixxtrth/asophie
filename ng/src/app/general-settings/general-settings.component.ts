import { Component, OnInit } from '@angular/core';
import { ModulesService } from '../service/modules.service';

@Component({
  selector: 'app-general-settings',
  templateUrl: './general-settings.component.html',
  styleUrls: ['./general-settings.component.scss']
})
export class GeneralSettingsComponent implements OnInit {
  private modulesList: Array<object>;
  constructor(
    private modules: ModulesService
  ) { }

  ngOnInit() {
    this.modulesList = [
      {
        'name': 'file',
        'title': 'ملف',
        'link': 'file',
        'level': 0
      },
      {
        'name': 'company',
        'title': 'الشركة',
        'link': 'company',
        'parent': 'file',
        'level': 9
      },
      {
        'name': 'companies',
        'title': 'اختار الشركة',
        'link': 'companies',
        'parent': 'company',
        'level': 9
      },
      {
        'name': 'companyInfo',
        'title': 'معلومات الشركة',
        'link': 'companyInfo',
        'parent': 'company',
        'level': 9
      },
      {
        'name': 'backup',
        'title': 'نسخه احتياطيه',
        'link': 'backup',
        'parent': 'file',
        'level': 9
      },
      {
        'name': 'retrieveBackup',
        'title': 'استرجاع نسخة احتياطية',
        'link': 'retrieveBackup',
        'parent': 'file',
        'level': 9
      },
      {
        'name': 'materialCard',
        'title': 'بطاقة المواد ',
        'link': 'materialCard',
        'level': 0
      },
      {
        'name': 'warehouseCard',
        'title': 'بطاقة مستودع ',
        'link': 'warehouseCard',
        'parent': 'materialCard',
        'level': 0
      },
      {
        'name': 'categories',
        'title': 'الاصناف',
        'link': 'categories',
        'parent': 'materialCard',
        'level': 0
      },
      {
        'name': 'materialsManagement',
        'title': 'ادارة المواد',
        'link': 'materialsManagement',
        'parent': 'materialCard',
        'level': 0
      },
      {
        'name': 'accountCard',
        'title': 'بطاقة الحساب',
        'link': 'accountCard',
        'level': 0
      },
      {
        'name': 'accountsGuide',
        'title': 'دليل الحسابات',
        'link': 'accountsGuide',
        'parent': 'accountCard',
        'level': 0
      }
      ,
      {
        'name': 'capitalAccount',
        'title': 'حساب راس المال',
        'link': 'capitalAccount',
        'parent': 'accountCard',
        'level': 0
      }
      ,
      {
        'name': 'Currencies',
        'title': 'العملات',
        'link': 'currencies',
        'parent': 'accountCard',
        'level': 0
      }
      ,
      {
        'name': 'Banks',
        'title': 'البنوك',
        'link': 'banks',
        'parent': 'accountCard',
        'level': 0
      }
      ,
      {
        'name': 'fundBox',
        'title': 'الصناديق',
        'link': 'fundBox',
        'parent': 'accountCard',
        'level': 0
      }
      ,
      {
        'name': 'customers',
        'title': 'الزبائن',
        'link': 'customers',
        'parent': 'accountCard',
        'level': 0
      }
      ,
      {
        'name': 'suppliers',
        'title': 'الموردين',
        'link': 'suppliers',
        'parent': 'accountCard',
        'level': 0
      }
      ,
      {
        'name': 'expenses',
        'title': 'المصاريف',
        'link': 'expenses',
        'parent': 'accountCard',
        'level': 0
      }
      ,
      {
        'name': 'inComes',
        'title': 'الايرادات',
        'link': 'inComes',
        'parent': 'accountCard',
        'level': 0
      }
      ,
      {
        'name': 'aggregateAccount',
        'title': 'حساب تجميعي',
        'link': 'aggregateAccount',
        'parent': 'accountCard',
        'level': 0
      }
      ,
      {
        'name': 'movements',
        'title': 'الحركات',
        'link': 'movements',
        'level': 0
      }
      ,
      {
        'name': 'registrationCertificate',
        'title': 'سند القيد',
        'link': 'registrationCertificate',
        'parent': 'movements',
        'level': 0
      }
      ,
      {
        'name': 'receiptVoucher',
        'title': 'سند القبض',
        'link': 'receiptVoucher',
        'parent': 'movements',
        'level': 0
      }
      ,
      {
        'name': 'exchangeVoucher',
        'title': 'سند الصرف',
        'link': 'exchangeVoucher',
        'parent': 'movements',
        'level': 0
      }
      ,
      {
        'name': 'invoices',
        'title': 'الفواتير',
        'link': 'invoices',
        'level': 0
      }
      ,
      {
        'name': 'purchasesBill',
        'title': 'فاتورة مشتريات',
        'link': 'invoices',
        'parent': 'invoices',
        'level': 0
      }
      ,
      {
        'name': 'billRecoupedPurchases',
        'title': 'فاتورة مرتجع مشتريات',
        'link': 'billRecoupedPurchases',
        'parent': 'invoices',
        'level': 0
      }
      ,
      {
        'name': 'salesBill',
        'title': 'فاتورة مبيعات',
        'link': 'salesBill',
        'parent': 'invoices',
        'level': 0
      }
      ,
      {
        'name': 'billOfReturn',
        'title': 'فاتورة مرتجع مبيعات',
        'link': 'billOfReturn',
        'parent': 'invoices',
        'level': 0
      }
      ,
      {
        'name': 'firstTimeStock',
        'title': 'مخزون اول المدة',
        'link': 'firstTimeStock',
        'parent': 'invoices',
        'level': 0
      }
      ,
      {
        'name': 'accountsReports',
        'title': 'تقارير الحسابات',
        'link': 'accountsReports',
        'level': 0
      }
      ,
      {
        'name': 'publicJournal',
        'title': 'اليومية العامة',
        'link': 'publicJournal',
        'parent': 'accountsReports',
        'level': 0
      }
      ,
      {
        'name': 'accountStatement',
        'title': 'كشف حساب',
        'link': 'accountStatement',
        'parent': 'accountsReports',
        'level': 0
      }
      ,
      {
        'name': 'accountsBalances',
        'title': 'ارصدة الحسابات',
        'link': 'accountsBalances',
        'parent': 'accountsReports',
        'level': 0
      }
      ,
      {
        'name': 'aggregateAccount',
        'title': 'حساب تجميعي',
        'link': 'aggregateAccount',
        'parent': 'accountsReports',
        'level': 0
      }
      ,
      {
        'name': 'finalAccounts',
        'title': 'الحسابات الختامية',
        'link': 'finalAccounts',
        'parent': 'accountsReports',
        'level': 0
      }
      ,
      {
        'name': 'billingReports',
        'title': 'تقارير الفواتير',
        'link': 'billingReports',
        'level': 0
      }
      ,
      {
        'name': 'dailyMovementOfBills',
        'title': 'الحركة اليومية للفواتير',
        'link': 'dailyMovementOfBills',
        'parent': 'billingReports',
        'level': 0
      }
      ,
      {
        'name': 'accountMovementByInvoice',
        'title': 'حركة الحساب بالفاتورة',
        'link': 'accountMovementByInvoice',
        'parent': 'billingReports',
        'level': 0
      }
      ,
      {
        'name': 'dailyMovementOfMaterials',
        'title': 'الحركة اليومية للمواد',
        'link': 'dailyMovementOfMaterials',
        'parent': 'billingReports',
        'level': 0
      }
      ,
      {
        'name': 'movementMaterial',
        'title': 'حركة مادة',
        'link': 'movementMaterial',
        'parent': 'billingReports',
        'level': 0
      }
      ,
      {
        'name': 'materialBalances',
        'title': 'ارصدة المواد',
        'link': 'materialBalances',
        'parent': 'billingReports',
        'level': 0
      }
      ,
      {
        'name': 'profitOfMaterials',
        'title': 'ارباح المواد',
        'link': 'profitOfMaterials',
        'parent': 'billingReports',
        'level': 0
      }
      ,
      {
        'name': 'auditing',
        'title': 'تدقيق الحسابات ',
        'link': 'auditing',
        'level': 0
      }
      ,
      {
        'name': 'expenses',
        'title': 'المصاريف',
        'link': 'expenses',
        'parent': 'auditing',
        'level': 0
      }
      ,
      {
        'name': 'sales',
        'title': 'المبيعات',
        'link': 'sales',
        'parent': 'auditing',
        'level': 0
      }
      ,
      {
        'name': 'customers',
        'title': 'الزبائن',
        'link': 'customers',
        'parent': 'auditing',
        'level': 0
      }
      ,
      {
        'name': 'suppliers',
        'title': 'الموردين',
        'link': 'suppliers',
        'parent': 'auditing',
        'level': 0
      }
      ,
      {
        'name': 'fundBox',
        'title': 'الصندوق',
        'link': 'fundBox',
        'parent': 'auditing',
        'level': 0
      }
      ,
      {
        'name': 'banks',
        'title': 'البنوك',
        'link': 'banks',
        'parent': 'auditing',
        'level': 0
      }
      ,
      {
        'name': 'purchasesForSale',
        'title': 'مشتريات بغرض البيع',
        'link': 'purchasesForSale',
        'parent': 'auditing',
        'level': 0
      }
      ,
      {
        'name': 'inComes',
        'title': 'الايرادات',
        'link': 'inComes',
        'parent': 'auditing',
        'level': 0
      }
      ,
      {
        'name': 'wages',
        'title': 'الاجور',
        'link': 'wages',
        'parent': 'auditing',
        'level': 0
      }
      ,
      {
        'name': 'specificAccount',
        'title': 'حساب محدد',
        'link': 'specificAccount',
        'parent': 'auditing',
        'level': 0
      }
      ,
      {
        'name': 'equations',
        'title': 'المعادلات',
        'link': 'equations',
        'level': 0
      }
      ,
      {
        'name': 'newEquations',
        'title': 'المعادلات',
        'link': 'newEquations',
        'parent': 'equations',
        'level': 0
      }
      ,
      {
        'name': 'viewEquations',
        'title': 'عرض المعادلة',
        'link': 'viewEquations',
        'parent': 'equations',
        'level': 0
      }
      ,
      {
        'name': 'settings',
        'title': 'الاعدادات',
        'link': 'settings',
        'level': 0
      }
      ,
      {
        'name': 'modules',
        'title': 'modules',
        'link': 'modules',
        'parent': 'settings',
        'level': 0
      }
      ,
      {
        'name': 'stores',
        'title': 'stores',
        'link': 'stores',
        'parent': 'settings',
        'level': 0
      }
      ,
      {
        'name': 'users',
        'title': 'المستخدمين',
        'link': 'users',
        'parent': 'settings',
        'level': 9
      }
      ,
      {
        'name': 'levels',
        'title': 'المستوى',
        'link': 'levels',
        'parent': 'settings',
        'level': 9
      }
      ,
      {
        'name': 'types',
        'title': 'الانوع',
        'link': 'types',
        'parent': 'settings',
        'level': 9
      }
    ];

    this.modules.subModulesLists(this.modulesList.filter((item) => {
      return (item['parent'] === 'settings' ? item : null);
    }));
  }

}
