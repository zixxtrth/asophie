import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserService } from '../service/user.service';

@Injectable()
export class LoginGuard implements CanActivate {
  constructor(private userService: UserService) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
     return true;
    // console.log(this.userService.isUserLoggedIn());
    // return this.userService.isUserLoggedIn() ? true : false;
  }
}
