import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InComesAuditingComponent } from './in-comes-auditing.component';

describe('InComesAuditingComponent', () => {
  let component: InComesAuditingComponent;
  let fixture: ComponentFixture<InComesAuditingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InComesAuditingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InComesAuditingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
