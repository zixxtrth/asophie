import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InComesComponent } from './in-comes.component';

describe('InComesComponent', () => {
  let component: InComesComponent;
  let fixture: ComponentFixture<InComesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InComesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InComesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
