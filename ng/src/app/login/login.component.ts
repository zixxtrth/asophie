import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private username;
  private password;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit() {
  }
  public login() {
    this.userService.login(this.username, this.password).subscribe(res => {
      this.userService.setUserLoggedIn(res);
      if (res.length) {
        this.router.navigate(['/dashboard']);
      } else {
        alert('username or password is incorrect! please check.');
      }
    });
  }
}
