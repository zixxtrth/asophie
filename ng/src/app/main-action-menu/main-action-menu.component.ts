import { Component, OnInit } from '@angular/core';
import { Error404Component } from '../error404/error404.component';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'actionMenu',
  templateUrl: './main-action-menu.component.html',
  styleUrls: ['./main-action-menu.component.scss']
})
export class MainActionMenuComponent implements OnInit {

  constructor(
    private tes: Error404Component
  ) { }

  ngOnInit() {
  }
  public add() {
    this.tes.test();
  }
}
