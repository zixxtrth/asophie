import { Component, OnInit } from '@angular/core';
import { ModulesService } from '../service/modules.service';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mainMenu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit {
  private modulesList: Array<Object>;
  private mainModules: Array<Object>;
  private subModules: Array<Object>;
  constructor(private modules: ModulesService) { }

  ngOnInit() {

    this.modulesList = [
      {
        'name': 'file',
        'title': 'ملف',
        'link': 'file',
        'level': 0
      },
      {
        'name': 'materialCard',
        'title': 'بطاقة المواد ',
        'link': 'materialCard',
        'level': 0
      },
      {
        'name': 'accountCard',
        'title': 'بطاقة الحساب',
        'link': 'accountCard',
        'level': 0
      },
      {
        'name': 'movements',
        'title': 'الحركات',
        'link': 'movements',
        'level': 0
      }
      ,
      {
        'name': 'invoices',
        'title': 'الفواتير',
        'link': 'invoices',
        'level': 0
      }
      ,
      {
        'name': 'accountsReports',
        'title': 'تقارير الحسابات',
        'link': 'accountsReports',
        'level': 0
      },
      {
        'name': 'billingReports',
        'title': 'تقارير الفواتير',
        'link': 'billingReports',
        'level': 0
      }
      ,
      {
        'name': 'auditing',
        'title': 'تدقيق الحسابات ',
        'link': 'auditing',
        'level': 0
      }
      ,
      {
        'name': 'equations',
        'title': 'المعادلات',
        'link': 'equations',
        'level': 0
      }
      ,
      {
        'name': 'settings',
        'title': 'الاعدادات',
        'link': 'settings',
        'level': 0
      }

    ];
    this.mainModules = this.modulesList;
  }

}
