import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialBalancesComponent } from './material-balances.component';

describe('MaterialBalancesComponent', () => {
  let component: MaterialBalancesComponent;
  let fixture: ComponentFixture<MaterialBalancesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialBalancesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialBalancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
