import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovementMaterialComponent } from './movement-material.component';

describe('MovementMaterialComponent', () => {
  let component: MovementMaterialComponent;
  let fixture: ComponentFixture<MovementMaterialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovementMaterialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovementMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
