import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovemntsComponent } from './movemnts.component';

describe('MovemntsComponent', () => {
  let component: MovemntsComponent;
  let fixture: ComponentFixture<MovemntsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovemntsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovemntsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
