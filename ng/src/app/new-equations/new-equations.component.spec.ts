import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewEquationsComponent } from './new-equations.component';

describe('NewEquationsComponent', () => {
  let component: NewEquationsComponent;
  let fixture: ComponentFixture<NewEquationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewEquationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEquationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
