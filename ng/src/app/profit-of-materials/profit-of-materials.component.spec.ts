import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfitOfMaterialsComponent } from './profit-of-materials.component';

describe('ProfitOfMaterialsComponent', () => {
  let component: ProfitOfMaterialsComponent;
  let fixture: ComponentFixture<ProfitOfMaterialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfitOfMaterialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfitOfMaterialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
