import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicJournalComponent } from './public-journal.component';

describe('PublicJournalComponent', () => {
  let component: PublicJournalComponent;
  let fixture: ComponentFixture<PublicJournalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicJournalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicJournalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
