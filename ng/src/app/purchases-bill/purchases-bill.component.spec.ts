import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasesBillComponent } from './purchases-bill.component';

describe('PurchasesBillComponent', () => {
  let component: PurchasesBillComponent;
  let fixture: ComponentFixture<PurchasesBillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasesBillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasesBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
