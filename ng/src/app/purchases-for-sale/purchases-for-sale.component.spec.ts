import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasesForSaleComponent } from './purchases-for-sale.component';

describe('PurchasesForSaleComponent', () => {
  let component: PurchasesForSaleComponent;
  let fixture: ComponentFixture<PurchasesForSaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasesForSaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasesForSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
