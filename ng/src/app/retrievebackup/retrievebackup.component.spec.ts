import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetrievebackupComponent } from './retrievebackup.component';

describe('RetrievebackupComponent', () => {
  let component: RetrievebackupComponent;
  let fixture: ComponentFixture<RetrievebackupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetrievebackupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetrievebackupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
