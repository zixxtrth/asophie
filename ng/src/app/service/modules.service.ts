import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class ModulesService {
  public subModules: Array<Object>;
  constructor(private _http: Http) { }

  public getAllModules(name) {
    return this._http.get('localhost:3000/modules/getModuleChildes/' + name).map(
      res => res.json()
    );
  }

  public subModulesLists(subMolde: Array<Object>) {
    this.subModules = subMolde;
  }

  public getSubModulesList() {
    return this.subModules;
  }


}
