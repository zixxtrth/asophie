import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { FlashMessagesService } from 'angular2-flash-messages';
@Injectable()
export class UserService {

  private userInfo: any = [];

  constructor(
    private _http: Http,
    private _flashMessagesService: FlashMessagesService
  ) { }

  public login(username, password) {
    return this._http.post('http://localhost:3000/users/login/', { 'username': username, 'password': password }).map(res => res.json());
  }

  public setUserLoggedIn(data) {
    this.userInfo = data;
  }

  public isUserLoggedIn() {
    if (!this.userInfo) {

      return false;
    }
    return true;
  }

  public getUserInfo() {
    return this.userInfo;
  }

}
