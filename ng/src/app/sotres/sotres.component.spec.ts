import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SotresComponent } from './sotres.component';

describe('SotresComponent', () => {
  let component: SotresComponent;
  let fixture: ComponentFixture<SotresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SotresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SotresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
