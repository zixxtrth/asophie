import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecificAccountComponent } from './specific-account.component';

describe('SpecificAccountComponent', () => {
  let component: SpecificAccountComponent;
  let fixture: ComponentFixture<SpecificAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecificAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecificAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
