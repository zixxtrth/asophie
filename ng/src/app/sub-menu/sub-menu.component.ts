import { Component, OnInit } from '@angular/core';
import { ModulesService } from '../service/modules.service';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
@Component({
  selector: 'app-sub-menu',
  templateUrl: './sub-menu.component.html',
  styleUrls: ['./sub-menu.component.scss']
})
export class SubMenuComponent implements OnInit {

  constructor(private _http: Http, private module: ModulesService) { }
  public moduleList: Array<Object>;
  ngOnInit() {
    this.moduleList = this.module.getSubModulesList();
  }

}
